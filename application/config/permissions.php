<?php

/**
 * Created by Jean Leal Silva.
 * User: Jean
 * Date: 03/01/2018
 * Time: 09:41
 */
$config['permission'] = array(
    'mapa_c' => array(
        'index' => array('admin', 'comum'),
        'save' => array('admin', 'comum'),
        'chamados' => array('admin'),
        'resolvido' => array('admin'),
        'ver_mapa' => array('admin'),
    ),
);

$config['exceptions'] = array(
    'login_c' => array(
        'auth',
        'logout',
        'enviar_email_recuperacao',
        'verificar_token',
        'recuperar_senha',
        'redefinir_senha',
    ),
    'protocolo_c' => array(
        'imprimir_recibo_temp',
        'imprimir_recibo',
    ),
);
