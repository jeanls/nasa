<style>
    #googleMap {
        position: absolute;
        width: 100%;
        left: 50px;
        height: 100%;
    }

    @media only screen and (max-width: 691px) {
        #googleMap {
            left: 0;
        }
    }
</style>
<div id="googleMap"></div>

<script>
    var map;
    function myMap() {
        var params = JSON.parse('<?=$result?>');
        initMap(params.lat, params.lng);
    }

    function initMap(latitude, longitude){
        var geocoder = new google.maps.Geocoder();
        var mapProp= {
            center:new google.maps.LatLng(latitude, longitude),
            zoom:12,
        };
        map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
        new google.maps.Marker({
            position:new google.maps.LatLng(latitude, longitude),
            map:map,
            icon: '<?=base_url('assets/img/user.png')?>'
        });
        var result = JSON.parse('<?=$result?>');
        var marker = new google.maps.Marker({
            position:new google.maps.LatLng(result.lat,result.lng),
            map:map,
            icon: result.tipo_ocorrencia == 1 ? '<?=base_url('assets/img/fire.png')?>' : '<?=base_url('assets/img/foco.png')?>'
            //animation:google.maps.Animation.BOUNCE
        });

        var content = "<h4>Local: </h4>" + result.endereco;
        content += "<br>";
        content += "<h4>Tipo: </h4>";
        content += result.tipo_ocorrencia === '1' ? 'Incêncio' : 'Foco';
        content += "<br>";
        content += "<h4>Reportado por: </h4>";
        content += '<?=$usuario->nome?>';
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', (function(marker,content,infowindow){
            return function() {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            };
        })(marker,content,infowindow));
    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6t8ePLXlhuxXe9eH8oINCRsEORNLjsaM&callback=myMap" async defer></script>