<?php

class Password_reset extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tabela = 'password_resets';
        $this->ordenacao = [
            'email' => 'desc',
        ];
    }

    public function inserir($dados) {
        return $this->db->insert($this->tabela, $dados);
    }
    
    public function remover($token) {
        $this->db->where('token', $token);
        return ($this->db->delete($this->tabela));
    }
    
    public function remover_email($email) {
        $this->db->where('email', $email);
        return ($this->db->delete($this->tabela));
    }

}
