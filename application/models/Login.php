<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Login class.
 *
 * @property Usuario_perfil usuario_perfil
 * @extends CI_Model
 */
class Login extends CI_Model {

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();

    }

    /**
     * create_user function.
     *
     * @access public
     * @param mixed $username
     * @param mixed $email
     * @param mixed $password
     */
    public function create_user($data) {
        $this->load->model('usuario_perfil');
        $data['senha'] = $this->hash_password( $data['senha']);
        $this->db->trans_start();
        $this->db->insert('usuario', $data);
        $usuario_id = $this->db->insert_id();
        $usuario_perfil = ['usuario_id' => $usuario_id, 'perfil_id' => 4];
        $this->usuario_perfil->inserir($usuario_perfil);
        $this->db->trans_complete();
        return $usuario_id;
    }

    /**
     * resolve_user_login function.
     *
     * @access public
     * @param mixed $username
     * @param mixed $password
     * @return bool true on success, false on failure
     */
    public function resolve_user_login($login, $senha) {
        $this->db->select('senha');
        $this->db->from('usuario');
        $this->db->where('login', $login);
        $this->db->where('ativo', 1);
        $hash = $this->db->get()->row();
        if(!$hash){
            return false;
        }
        return $this->verify_password_hash($senha, $hash->senha);
    }

    /**
     * get_user_id_from_username function.
     *
     * @access public
     * @param mixed $username
     * @return int the user id
     */
    public function get_user_id_from_username($login) {

        $this->db->select('id');
        $this->db->from('usuario');
        $this->db->where('login', $login);

        return $this->db->get()->row('id');

    }

    /**
     * get_user function.
     *
     * @access public
     * @param mixed $user_id
     * @return object the user object
     */
    public function get_user($user_id) {
        $this->db->from('usuario');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();
    }

    /**
     * hash_password function.
     *
     * @access private
     * @param mixed $password
     * @return string|bool could be a string on success, or bool false on failure
     */
    public function hash_password($password) {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * verify_password_hash function.
     *
     * @access private
     * @param mixed $password
     * @param mixed $hash
     * @return bool
     */
    public function verify_password_hash($password, $hash) {
        return password_verify($password, $hash);
    }

    public function alterar_senha($usuario_id, $nova_senha){
        $nova_senha = $this->hash_password($nova_senha);
        $this->db->where('id', $usuario_id);
        $this->db->update('usuario', ['senha' => $nova_senha]);
    }
}