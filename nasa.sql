/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : nasa

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 21/10/2018 08:25:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chamado
-- ----------------------------
DROP TABLE IF EXISTS `chamado`;
CREATE TABLE `chamado`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usuario_id` int(10) NOT NULL,
  `lat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lng` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `data_cadastro` datetime(0) NOT NULL,
  `endereco` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `imagem` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipo_ocorrencia` tinyint(1) NOT NULL,
  `aberto` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of chamado
-- ----------------------------
INSERT INTO `chamado` VALUES (1, 1, '-12.969457648870481', '-38.512487077211404', '0000-00-00 00:00:00', 'Avenida da França, 381 - Comercio, Salvador - BA, 40015-080, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (2, 1, '-12.969644534458753', '-38.512438797449136', '0000-00-00 00:00:00', 'Avenida da França, 381 - Comercio, Salvador - BA, 40015-080, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (3, 1, '-12.969512538358535', '-38.511829936002755', '0000-00-00 00:00:00', 'Av. Antônio Carlos Magalhães, 237 - Pituba, BA, 40280-000, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (4, 1, '-12.969289059652914', '-38.512026407813096', '2018-10-19 21:57:15', 'Av. da França, 414 - Comercio, Salvador - BA, 40010-000, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (5, 1, '-12.969113484427927', '-38.50393949282761', '2018-10-19 21:58:17', 'Av. José Joaquim Seabra, 465 - Sete portas, Salvador - BA, 40025-001, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (6, 1, '-12.961270587922318', '-38.61036770073241', '2018-10-20 00:24:51', 'Estr. da Rodagem, 26 - Lot. Parque das Mangueiras, Vera Cruz - BA, 44470-000, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (7, 1, '-12.94960397219528', '-38.631825772851585', '2018-10-20 00:25:14', 'Vera Cruz - BA, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (8, 1, '-12.922500610851262', '-38.46359762343752', '2018-10-20 00:25:22', 'R. Varsóvia, 700 - Granjas Rurais Pres. Varg, Salvador - BA, 41230-025, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (9, 1, '-13.004805398647477', '-38.5229924598633', '2018-10-20 00:25:32', 'Condomínio Edifício São Francisco - R. Plínio Moscoso, 1 - Chame-Chame, Salvador - BA, 40140-005, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (10, 1, '-12.896732959325956', '-38.48265203627932', '2018-10-20 00:26:10', 'R. Manoel Monte de Cima, 101 - Plataforma, Salvador - BA, 40717-332, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (11, 1, '-12.90078611873432', '-38.4121291489746', '2018-10-20 00:28:35', 'Cajazeiras, Salvador - BA, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (12, 1, '-13.017553945786453', '-38.702923521533194', '2018-10-20 00:28:43', 'Vera Cruz - BA, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (13, 1, '-12.932576478259834', '-38.395306334033194', '2018-10-20 00:36:27', '002255 - Luís Viana - Alphaville II, Salvador - BA, 41680-400, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (14, 1, '-12.99479558507158', '-38.635796684521495', '2018-10-20 00:36:36', 'R. Sr. do Bonfim, 121 - Condomínio Passárgada, Vera Cruz - BA, 44470-000, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (15, 1, '-12.966693439969575', '-38.64437975336915', '2018-10-20 00:38:35', 'Vera Cruz - BA, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (16, 1, '-12.970304863621026', '-38.441089883056634', '2018-10-20 00:38:43', 'Av. Luís Viana, 1600 - Imbuí, Salvador - BA, 41720-200, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (17, 1, '-12.995061322219774', '-38.47336222192382', '2018-10-20 00:38:46', 'R. Monsenhor Eugênio Veiga, 98 - Itaigara, Salvador - BA, 41815-120, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (18, 1, '-12.917073462335862', '-38.401024681691865', '2018-10-20 00:45:21', 'R. Pequeno Príncipe, 257 - Nova Brasília, Salvador - BA, 41301-110, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (19, 1, '-12.958199606958415', '-38.43096976015005', '2018-10-20 14:06:54', 'Av. Luís Viana, 3476 - Imbuí, Salvador - BA, 40301-110, Brasil', '', 1, 0);
INSERT INTO `chamado` VALUES (20, 1, '-12.972777617387337', '-38.508070958311976', '2018-10-20 14:35:43', 'R. Maciel de Baixo, 39 - Pelourinho, Salvador - BA, 40026-240, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (21, 10, '-12.878910909041233', '-38.42976536465767', '2018-10-20 16:48:08', 'R. Caramuru, 59 - Valéria, Salvador - BA, 41311-346, Brasil', '', 2, 0);
INSERT INTO `chamado` VALUES (22, 10, '-12.916762265143348', '-38.40053753057566', '2018-10-20 17:26:07', 'Unnamed Road - Cajazeiras, Salvador - BA, 41301-110, Brasil', '', 1, 1);
INSERT INTO `chamado` VALUES (23, 10, '-12.906723005698403', '-38.44722942510691', '2018-10-20 17:26:12', 'R. Cid Moreira, 135 - Dom Avelar, Salvador - BA, 41290-000, Brasil', '', 2, 1);
INSERT INTO `chamado` VALUES (24, 10, '-12.943866249815246', '-38.44173626104441', '2018-10-20 17:26:16', 'Arenoso, Salvador - BA, 41211-221, Brasil', '', 1, 1);
INSERT INTO `chamado` VALUES (25, 12, '-15.102982869762192', '-43.301365744314865', '2018-10-21 13:16:20', 'Pai Pedro - MG, 39517-000, Brasil', '', 1, 1);
INSERT INTO `chamado` VALUES (26, 12, '-14.902137360264181', '-40.85365885382055', '2018-10-21 13:17:03', 'Anel Rodoviário Jadiel Matos Leste - Aírton Senna, Vitória da Conquista - BA, Brasil', '', 1, 1);
INSERT INTO `chamado` VALUES (27, 12, '-14.85445463317505', '-40.79846195988705', '2018-10-21 13:17:38', 'R. Um, 2062 - Primavera, Vitória da Conquista - BA, Brasil', '', 2, 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `token` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('jean.leal22@gmail.com', 'a97fc344fc0326acf2b3110eaa9e485d', '2018-03-29 14:48:17');

-- ----------------------------
-- Table structure for perfil
-- ----------------------------
DROP TABLE IF EXISTS `perfil`;
CREATE TABLE `perfil`  (
  `nome` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of perfil
-- ----------------------------
INSERT INTO `perfil` VALUES ('admin', 1);
INSERT INTO `perfil` VALUES ('comum', 4);

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bairro` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cep` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cidade` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `complemento` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contato` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `data_atualizacao` datetime(0) NOT NULL,
  `data_cadastro` datetime(0) NOT NULL,
  `logradouro` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nome` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `login` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `senha` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT 1,
  `sexo` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lng` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `login`(`login`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES (1, 'Brasil', '45051-295', 'Vitória da Conquista', 'BL 3 AP 103', '(77) 98842-1244', '2018-05-07 15:57:23', '2018-03-27 10:37:12', 'Av Frei Benjamin 2', 'Jean Leal Silva', 'admin', '$2y$2y$10$Oh8/ATDVoQzvE8A92f5.suaCMbOUTFEaidRRocZReDXDgwesZnJjy', 'admin@admin.coms', 1, 'M', NULL, NULL);
INSERT INTO `usuario` VALUES (7, NULL, NULL, NULL, NULL, NULL, '2018-10-19 15:25:35', '2018-10-19 15:25:35', NULL, 'TESTESASDHJH', 'teste@teste.com', '$2y$10$GfhGytybe1RwFQSo0jtyKe74l5hjr21kJoKwcHAOHjZQGA0uhr5NC', 'teste@teste.com', 1, NULL, NULL, NULL);
INSERT INTO `usuario` VALUES (8, NULL, NULL, NULL, NULL, NULL, '2018-10-20 14:43:40', '2018-10-20 14:43:40', NULL, 'ARNALDO SANTANA', 'arnaldo@a.com.br', '$2y$10$n5KMZ/i4XVuNL.sMuTGH4ebH3I3jDy.x8aRTdL/XTDfYz/3MKEmbm', 'arnaldo@a.com.br', 1, NULL, NULL, NULL);
INSERT INTO `usuario` VALUES (9, NULL, NULL, NULL, NULL, NULL, '2018-10-20 15:12:56', '2018-10-20 15:12:56', NULL, 'ADMIN', 'admin@admin.com', '$2y$10$a5iDsN7hiOBmb7EsQnAhJOHQCsUklMCFzHbkvJGacOnM7IcqBAdt2', 'admin@admin.com', 1, NULL, NULL, NULL);
INSERT INTO `usuario` VALUES (10, NULL, NULL, NULL, NULL, NULL, '2018-10-20 16:11:22', '2018-10-20 16:11:22', NULL, 'TESTE TESTE', 'teste1@teste.com', '$2y$10$UDCvkWA8Qz6SdV2PhZRfPuOB8xeb6cEPYSBv/B69Ui.ApzIVCOcDC', 'teste1@teste.com', 1, NULL, NULL, NULL);
INSERT INTO `usuario` VALUES (11, NULL, NULL, NULL, NULL, NULL, '2018-10-20 22:09:36', '2018-10-20 22:09:36', NULL, 'TERRA PLANA', 'terra@plana.com.br', '$2y$10$Oh8/ATDVoQzvE8A92f5.suaCMbOUTFEaidRRocZReDXDgwesZnJjy', 'terra@plana.com.br', 1, NULL, NULL, NULL);
INSERT INTO `usuario` VALUES (12, NULL, NULL, NULL, NULL, '12345', '2018-10-21 13:14:37', '2018-10-21 13:14:37', NULL, 'TESTE', '123@123.com', '$2y$10$O04FLrwxF1dVUpDI5ZPm5.P8oIVTzymNJFhuRx6Mb/26b7l6nqxpm', '123@123.com', 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for usuario_perfil
-- ----------------------------
DROP TABLE IF EXISTS `usuario_perfil`;
CREATE TABLE `usuario_perfil`  (
  `usuario_id` int(10) UNSIGNED NOT NULL,
  `perfil_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`usuario_id`) USING BTREE,
  INDEX `perfil_id`(`perfil_id`) USING BTREE,
  CONSTRAINT `usuario_perfil_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `usuario_perfil_ibfk_2` FOREIGN KEY (`perfil_id`) REFERENCES `perfil` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuario_perfil
-- ----------------------------
INSERT INTO `usuario_perfil` VALUES (1, 1);
INSERT INTO `usuario_perfil` VALUES (9, 1);
INSERT INTO `usuario_perfil` VALUES (10, 1);
INSERT INTO `usuario_perfil` VALUES (7, 4);
INSERT INTO `usuario_perfil` VALUES (8, 4);
INSERT INTO `usuario_perfil` VALUES (11, 4);
INSERT INTO `usuario_perfil` VALUES (12, 4);

SET FOREIGN_KEY_CHECKS = 1;
