<?php

$config = array(
    'curso_c' => array(
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required|trim',
        )
    ),
    'instituicao_c' => array(
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required|trim',
        )
    ),
    'tipo_evento_c' => array(
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required|trim',
        )
    ),
    'tipo_item_c' => array(
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required|trim',
        )
    ),
    'evento_c' => array(
        array(
            'field' => 'ativo',
            'label' => 'Ativo',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'bairro',
            'label' => 'Bairro',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'cep',
            'label' => 'CEP',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'cidade',
            'label' => 'Cidade',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'data_inicio',
            'label' => 'Data de início',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'data_fim',
            'label' => 'Data fim',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'descricao',
            'label' => 'Descrição',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'local',
            'label' => 'Local',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'logradouro',
            'label' => 'Logradouro',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'tipo_evento_id',
            'label' => 'Tipo de evento',
            'rules' => 'required|trim',
        ),
    ),
    'facilitador_c' => array(
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required|trim',
        )
    ),
    'perfil_c' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|trim|is_unique[usuario.email]'
        )
    ),
    'perfil_c_2' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|trim',
        )
    ),
    'item_c' => array(
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'carga_horaria',
            'label' => 'Carga Horária',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'descricao',
            'label' => 'Descrição',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'local',
            'label' => 'Local',
            'rules' => 'required|trim',
        ),
        array(
            'field' => 'data_realizacao',
            'label' => 'Data realização',
            'rules' => 'required|trim',
        ),
    ),
);