<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @property login login
 * @property Usuario usuario
 * @property Password_reset password_reset
 * @extends CI_Controller
 */
class Login_c extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('login');
        $this->load->model('usuario');
        $this->load->model('password_reset');
    }

    public function index() {
        
    }

    /**
     * register function.
     *
     * @access public
     * @return void
     */
    public function registro() {
        $this->load->view('login/registro', []);
    }

    public function novo_usuario(){
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[usuario.email]', array('is_unique' => 'Este email já está sendo utilizado por outra pessoa'));
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('senha_confirm', 'Confirmar Senha', 'trim|required|min_length[6]|matches[senha]', array('matches' => 'As senhas não conferem'));
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(['nome', 'email', 'senha', 'contato']);
            $data['data_atualizacao'] = $data['data_cadastro'] = date("Y-m-d H:i:s");
            $data['nome'] = strtoupper($data['nome']);
            $data['login'] = $data['email'];
            $id_usuario = $this->login->create_user($data);
            if ($id_usuario) {
                $this->load->model('usuario');
                $user = $this->usuario->buscar_row(['where' => ['id' => $id_usuario]]);
                $_SESSION['user_id'] = (int) $id_usuario;
                $_SESSION['username'] = (string) $user->login;
                $_SESSION['logged_in'] = (bool) true;
                $_SESSION['name'] = $user->nome;
                redirect('');
            } else {
                adicionar_alerta('warning', "Não foi possível criar seu usuário no momento");
            }
        } else {
            adicionar_alerta('danger', validation_errors());
            $this->registro();
        }
    }

    /**
     * login function.
     *
     * @access public
     * @return void
     */
    public function auth() { 

        // create the data object
        $data = new stdClass();

        // set validation rules
        $this->form_validation->set_rules('login', 'Email', 'required|trim');
        $this->form_validation->set_rules('password', 'Senha', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('login/auth');
        } else {

            // set variables from the form
            $username = $this->input->post('login');
            $password = $this->input->post('password');

            if ($this->login->resolve_user_login($username, $password)) {

                $user_id = $this->login->get_user_id_from_username($username);
                $user = $this->login->get_user($user_id);

                // set session user datas
                $_SESSION['user_id'] = (int) $user->id;
                $_SESSION['username'] = (string) $user->login;
                $_SESSION['logged_in'] = (bool) true;
                $_SESSION['name'] = $user->nome;
                // user login ok
                redirect('');
            } else {
                adicionar_alerta('danger', 'Login ou senha incorretos, ou sua conta pode estar suspensa');
                $this->load->view('login/auth');
            }
        }
    }

    public function denied() {
        $this->load->view('denied');
    }

    /**
     * logout function.
     *
     * @access public
     * @return void
     */
    public function logout() {
        // create the data object
        $data = new stdClass();

        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            // user logout ok
            $this->load->view('login/auth');
        } else {
            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            redirect('login/auth');
        }
    }
}
