<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Usuario_model
 * @property login login
 */
class Usuario extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tabela = 'usuario';
        $this->ordenacao = [
            $this->tabela.'.nome' => 'asc',
        ];
    }

    public function listar($limit, $offset) {
        $this->db->select('nome, matricula, login, email, id, ativo');
        $this->db->from($this->tabela);
        $this->db->limit($limit, $offset);
        return $this->db->get()->result();
    }

    public function inserir_crypt_senha($dados) {
        $this->load->model('login');
        $dados['senha'] = $this->login->hash_password($dados['senha']);
        $this->db->insert($this->tabela, $dados);
        $id = $this->db->insert_id();
        return $id;
    }

    public function buscar_por_termos($termos, $paginacao) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->or_like('nome', $termos);
        $this->db->or_like('email', $termos);
        $this->db->or_like('login', $termos);
        $this->db->limit(30, $paginacao['pagina']);
        return $this->db->get()->result();
    }

    public function buscar_por_termos_count($termos) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->or_like('nome', $termos);
        $this->db->or_like('email', $termos);
        $this->db->or_like('login', $termos);
        return $this->db->get()->num_rows();
    }

    public function getPerfis($usuario_id) {
        $result = $this->db->select("perfil_id")->from("usuario_perfil")->where("usuario_id", $usuario_id)->get()->result();
        $id_perfis = array();
        foreach ($result as $row) {
            array_push($id_perfis, $row->perfil_id);
        }
        if (!$id_perfis) {
            return array();
        }
        return $this->db->select("*")->from("perfil")->where_in("id", $id_perfis)->get()->result();
    }

    public function buscar_com_relacoes($filtros = array(), $paginacao = array()) {
        $this->db->select($this->tabela.".*");
        $this->join_secretaria($filtros);
        $this->get_filtros($filtros);
        $this->get_pagincacao($paginacao);
        $this->order_by();
        $result = $this->db->get($this->tabela)->result();

        $this->load->model('setor');
        $usuarios = array();

        foreach ($result as $usuario) {
            $usuario->setor = $this->setor->buscar_com_relacoes(['where' => ['id' => $usuario->setor_id]])[0];
            $usuarios[] = $usuario;
        }

        return $usuarios;
    }

    public function buscar_array($filters = array()) {
        $usuarios = $this->buscar_com_relacoes($filters);
        $dados = ['' => '-- Selecione --'];
        foreach ($usuarios as $usuario) {
            $dados[$usuario->id] = $usuario->nome . " - " . $usuario->setor->nome;
        }
        return $dados;
    }
    
    public function validar($email_login, $password) {
        $this->db->select('senha');
        $this->db->where('login', $email_login)
                ->or_where('email', $email_login);
        $user = $this->db->get('usuarios')->result();
        if ($user) { //se encontrar usuario com o login ou email
            return password_verify($password, $user[0]->password); //compara a senha enviada com a armazenada no banco
        } else { //caso não encontre usuario com o login ou email
            return FALSE;
        }
    }

    public function buscar_email($id) {
        $this->db->select('email');
        $this->db->where('id', $id);
        return $this->db->get('usuarios')->result();
    }

    public function buscar_usuario_secretaria_setor($busca, $paginacao) {
        $this->get_pagincacao($paginacao);
        $this->get_query_busca($busca);
        return $this->db->get()->result();
    }

    public function total_busca($busca) {
        $this->get_query_busca($busca);
        return $this->db->count_all_results();
    }
    
    private function join_secretaria(&$filters = array()){
        if (isset ($filters['secretaria_usuario'])){
            $this->db->join('setores as set', 'set.id = usuarios.setor_id')
                    ->join('secretarias as sec', 'sec.id = set.secretaria_id')
                    ->where('sec.id', $filters['secretaria_usuario']);
            unset($filters['secretaria_usuario']);
        }
    }
    
    private function get_query_busca($busca) {
        $this->db->select('usuarios.nome as usuario,usuarios.cargo, secretarias.nome as secretaria,setores.nome as setor,secretarias.descricao as descricao_secretaria');
        $this->db->from('usuarios');
        $this->db->join('setores', 'setores.id = usuarios.setor_id');
        $this->db->join('secretarias', 'secretarias.id = setores.secretaria_id');
        $this->db->where('usuarios.ativo', 1);
        $this->db->like('usuarios.nome', $busca);
        $this->db->or_like('setores.nome', $busca);
        $this->db->or_like('secretarias.nome', $busca);
        $this->db->or_like('secretarias.descricao', $busca);
    }

    public function remove_usuarios_perfil($usuarios, $exclude_perfis){
        foreach ($usuarios as $id => $nome){
            $perfis = $this->getPerfis($id);
            $macth = false;
            foreach ($perfis as $perfil){
                foreach ($exclude_perfis as $exclude_perfil){
                    if($perfil->nome == $exclude_perfil){
                        $macth = true;
                    }
                }
            }
            if($macth){
                unset($usuarios[$id]);
            }
        }
        return $usuarios;
    }

    public function get_usuario_app_cidada(){
        $result = $this->buscar_row([
           'where' => ['login' => 'conquistacidada']
        ]);
        return $result;
    }

    public function ajaxUsuarios($q){
        $perfis = array(
            'admin',
            'cadastro'
        );
        $this->db->select('u.id, u.nome, p.nome as perfil, s.nome as setor, sa.nome as secretaria')->from('usuarios u');
        $this->db->join('usuarios_perfis up', 'up.usuario_id = u.id', 'left');
        $this->db->join('perfis p', 'up.perfil_id = p.id', 'left');
        $this->db->join('setores s', 'u.setor_id = s.id', 'left');
        $this->db->join('secretarias sa', 's.secretaria_id = sa.id');
        $this->db->where_not_in('p.nome', $perfis);
        $this->db->group_start();
            $this->db->or_like('u.nome', $q, 'both');
            $this->db->or_like('s.nome', $q, 'both');
            $this->db->or_like('sa.nome', $q, 'both');
        $this->db->group_end();
        $result = $this->db->get()->result();
        $lq = $this->db->last_query();
        $retorno = array();
        foreach ($result as $item){
            $retorno[$item->id] = $item->nome.' / '.$item->setor.' / '.$item->secretaria;
        }
        return $retorno;
    }

    public function get_usuarios_revisores($ids){
        $perfis = array(
            'admin',
            'cadastro'
        );
        $this->db->select('u.id, u.nome, p.nome as perfil, s.nome as setor, sa.nome as secretaria')->from('usuarios u');
        $this->db->join('usuarios_perfis up', 'up.usuario_id = u.id', 'left');
        $this->db->join('perfis p', 'up.perfil_id = p.id', 'left');
        $this->db->join('setores s', 'u.setor_id = s.id', 'left');
        $this->db->join('secretarias sa', 's.secretaria_id = sa.id');
        $this->db->where_not_in('p.nome', $perfis);
        $this->db->where_in('u.id', $ids);
        $result = $this->db->get()->result();
        $retorno = array();
        foreach ($result as $item){
            $retorno[$item->id] = $item->nome.' / '.$item->setor.' / '.$item->secretaria;
        }
        return $retorno;
    }
}
