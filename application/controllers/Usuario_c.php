<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property usuario usuario
 * @property setor setor
 * @property perfil perfil
 * @property usuarioPerfil usuarioPerfil
 * @property Usuario_vinculo usuario_vinculo
 */
class Usuario_c extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('usuario');
        $this->load->model('password_reset');
        $this->load->model('perfil');
        $this->load->model('login');
        $this->load->model('usuarioPerfil');
        $this->load->model('usuario_vinculo');
    }

    public function index() {
        $termos = $this->input->get('q');
        $query = [];
        if (!empty($termos)) {
            $dados['busca'] = $termos;
            $query['or_like'] = [
                'nome' => $termos,
                'matricula' => $termos,
                'email' => $termos,
                'login' => $termos,
            ];
        }
        $paginacao = $this->get_query_paginacao($this->input->get('page'));
        $dados['usuarios'] = $this->usuario->buscar($query, $paginacao);
        foreach ($dados['usuarios'] as $usuario) {
            $usuario->perfis = $this->usuarioPerfil->perfis($usuario->id, true);
        }
        $this->initPagination('usuarios', $this->usuario->total($query));
        $this->carregar_pagina('usuario/index', $dados);
    }

    public function criar() {
        $this->load->model('setor');
        $dados['setores'] = $this->setor->buscar_array(['where' => ['ativo' => 1]]);
        $dados['perfis'] = $this->perfil->buscar_array(['where' => ['id !=' => 1]]);
        $this->carregar_pagina('usuario/criar', $dados);
    }

    public function salvar() {
        if ($this->form_validation->run('usuario_c') === TRUE) {
            $usuario = $this->input->post(array('nome', 'matricula', 'cpf', 'email', 'telefone', 'login', 'setor_id', 'senha', 'cargo'));
            $usuario['data_cadastro'] = date("Y-m-d H:i:s");
            $usuario['data_atualizacao'] = date("Y-m-d H:i:s");
            $usuario['ativo'] = 1;
            $usuario_id = $this->usuario->inserir_crypt_senha(array_filter($usuario, 'strlen'));
            if ($usuario_id) {
                $perfil = $this->input->post('perfil');
                $dados = array(
                    "usuario_id" => $usuario_id,
                    "perfil_id" => $perfil
                );
                $this->usuarioPerfil->inserir($dados);
                $vinculos = $this->input->post('vinculos');
                if(!empty($vinculos)){
                    foreach ($vinculos as $vinculo){
                        $usuario_vinculo = array(
                            'usuario_id' => $usuario_id,
                            'vinculo_id' => $vinculo,
                            'data_cadastro' => date('Y-m-d H:i:s')
                        );

                        $vinculo_usuario = array(
                            'usuario_id' => $vinculo,
                            'vinculo_id' => $usuario_id,
                            'data_cadastro' => date('Y-m-d H:i:s')
                        );
                        $this->usuario_vinculo->inserir($usuario_vinculo);
                        $this->usuario_vinculo->inserir($vinculo_usuario);
                    }
                }
                adicionar_alerta('alert alert-success', 'Usuário cadastrado com sucesso');
                redirect('usuarios');
            }
        } else {
            adicionar_alerta('alert alert-danger', validation_errors());
            $this->criar();
        }
    }

    public function atualizar() {
        $dados = $this->input->post(array('nome', 'matricula', 'email', 'telefone', 'login', 'setor_id', 'id', 'cpf', 'cargo', 'senha'));
        $usuarioBd = $this->usuario->buscar(['where' => ['id' => $dados['id']]])[0];
        $this->form_validation_atualizar($dados, $usuarioBd);

        if ($this->form_validation->run() === TRUE) {
            $dados['data_atualizacao'] = date('Y-m-d H:i:s');
            if (!empty($dados['senha'])) {
                $dados['senha'] = password_hash($dados['senha'], PASSWORD_BCRYPT);
            } else {
                unset($dados['senha']);
            }
            if ($this->usuario->atualizar($usuarioBd->id, $dados)) {
                $this->usuarioPerfil->removeAll($dados['id']);
                $perfil = $this->input->post('perfil');
                $dados = array(
                    "usuario_id" => $dados['id'],
                    "perfil_id" => $perfil
                );
                $this->usuarioPerfil->inserir($dados);
                $vinculos = $this->input->post('vinculos');
                $this->usuario_vinculo->remover_vinculos($usuarioBd->id);
                if(!empty($vinculos)){
                    foreach ($vinculos as $vinculo){
                        $usuario_vinculo = array(
                            'usuario_id' => $usuarioBd->id,
                            'vinculo_id' => $vinculo,
                            'data_cadastro' => date('Y-m-d H:i:s')
                        );

                        $vinculo_usuario = array(
                            'usuario_id' => $vinculo,
                            'vinculo_id' => $usuarioBd->id,
                            'data_cadastro' => date('Y-m-d H:i:s')
                        );
                        $this->usuario_vinculo->inserir($usuario_vinculo);
                        $this->usuario_vinculo->inserir($vinculo_usuario);
                    }
                }
                adicionar_alerta('alert alert-success', 'Usuário atualizado com sucesso');
                redirect('usuarios');
            }
        } else {
            adicionar_alerta('alert alert-danger', validation_errors());
            $this->editar($usuarioBd->id);
//            redirect('usuarios/editar/'.$usuarioBd->id);
        }
    }

    public function atualizar_status($usuario_id) {
        if ($this->form_validation->run('atualizar_status')) {
            $result = $this->usuario->buscar_row(['where' => ['id' => $usuario_id]]);
            if ($result) {
                $result->ativo = $this->input->post('status');
                $this->usuario->atualizar($result->id, $result);
                adicionar_alerta('alert alert-success', 'Status alterado.');
            }
        } else {
            adicionar_alerta('alert alert-danger', validation_errors());
        }
        redirect('usuarios');
    }

    public function editar($usuario_id) {
        $this->load->model('setor');
        $result = $this->usuario->buscar(['where' => ['id' => $usuario_id]]);
        $dados['usuario'] = isset($result) ? $result[0] : '';
        $dados['setores'] = $this->setor->buscar_array(['where' => ['ativo' => 1]]);
        $dados['usuario']->perfis = $this->usuarioPerfil->perfis($usuario_id, false);
        $dados['perfis'] = $this->perfil->buscar_array(['where' => ['id !=' => 1]]);
        $vinculos = $this->usuario_vinculo->buscar(['where' => ['usuario_id' => $usuario_id]]);
        $rev = array();
        foreach ($vinculos as $vinculo){
            array_push($rev, $vinculo->vinculo_id);
        }
        if($rev){
            $dados['rev'] = $this->usuario->get_usuarios_revisores($rev);
        }
        $this->carregar_pagina('usuario/editar', $dados);
    }

    private function form_validation_atualizar($dados, $usuarioBd) {
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('setor_id', 'Setor', 'required');
        $this->form_validation->set_rules('telefone', 'Telefone', 'required');
//        $this->form_validation->set_rules('cargo', 'Cargo', 'required');

        if (!empty($dados['senha'])) {
            $this->form_validation->set_rules('senha', 'Senha', 'trim|required|min_length[6]');
            $this->form_validation->set_rules('senha_confirm', 'Confirmação de senha', 'trim|required|min_length[6]|matches[senha]');
        }

        if ($dados['email'] == $usuarioBd->email) {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        } else {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[usuarios.email]');
        }

        if ($dados['login'] === $usuarioBd->login) {
            $this->form_validation->set_rules('login', 'Login', 'required');
        } else {
            $this->form_validation->set_rules('login', 'Login', 'required|is_unique[usuarios.login]');
        }

        if ($dados['cpf'] === $usuarioBd->cpf) {
            $this->form_validation->set_rules('cpf', 'CPF', 'required');
        } else {
            $this->form_validation->set_rules('cpf', 'CPF', 'required');
        }

        if ($dados['matricula'] === $usuarioBd->matricula) {
            $this->form_validation->set_rules('matricula', 'Matrícula', 'required');
        } else {
            $this->form_validation->set_rules('matricula', 'Matrícula', 'required');
        }
    }

    public function alterar_senha() {
        $this->carregar_pagina('usuario/alterar_senha');
    }

    public function atualizar_senha() {
        $dados = $this->input->post();
        $usuario_id = $this->session->user_id;
        $usuario = $this->usuario->buscar(['where' => ['id' => $usuario_id]])[0];

        if ($this->form_validation->run() === TRUE) {
            if ($this->login->resolve_user_login($usuario->login, $dados['old_password'])) {
                $dados_usuario['senha'] = password_hash($dados['password'], PASSWORD_BCRYPT);
                $dados_usuario['data_atualizacao'] = date("Y-m-d H:i:s");
                if ($this->usuario->atualizar($usuario_id, $dados_usuario)) {
                    adicionar_alerta('alert alert-success', 'Senha alterada com sucesso.');
                    redirect('usuarios/alterar-senha');
                }
            } else {
                adicionar_alerta('alert alert-danger', 'Senha atual incorreta');
                redirect('usuarios/alterar-senha');
            }
        } else {
            adicionar_alerta('alert alert-danger', validation_errors());
            redirect('usuarios/alterar-senha');
        }
    }

    public function editar_cadastro() {
        $dados["usuario"] = $this->usuario->buscar(["where" => ["id" => $this->session->user_id]])[0];
        $this->carregar_pagina('usuario/editar_cadastro', $dados);
    }

    public function atualizar_cadastro() {
        $usuario_id = $this->session->user_id;
        $usuarioBd = $this->usuario->buscar(['where' => ['id' => $usuario_id]])[0];
        $dados = $this->input->post();
        $this->form_validation_atualizar_cadastro($dados, $usuarioBd);
        if ($this->form_validation->run() === TRUE) {
            $dados['data_atualizacao'] = date("Y-m-d H:i:s");
            $this->usuario->atualizar($usuario_id, $dados);
            adicionar_alerta('alert alert-success', 'Dados atualizados com sucesso.');
            redirect('usuarios/editar-cadastro');
        } else {
            adicionar_alerta('alert alert-danger', validation_errors());
            redirect('usuarios/editar-cadastro');
        }
    }

    public function form_validation_atualizar_cadastro($dados, $usuarioBd) {
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('telefone', 'Telefone', 'required');

        if ($dados['email'] == $usuarioBd->email) {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        } else {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[usuarios.email]');
        }

        if ($dados['login'] === $usuarioBd->login) {
            $this->form_validation->set_rules('login', 'Login', 'required');
        } else {
            $this->form_validation->set_rules('login', 'Login', 'required|is_unique[usuarios.login]');
        }

        if ($dados['cpf'] === $usuarioBd->cpf) {
            $this->form_validation->set_rules('cpf', 'CPF', 'required');
        } else {
            $this->form_validation->set_rules('cpf', 'CPF', 'required|is_unique[usuarios.cpf]');
        }

        if ($dados['matricula'] === $usuarioBd->matricula) {
            $this->form_validation->set_rules('matricula', 'Matrícula', 'required');
        } else {
            $this->form_validation->set_rules('matricula', 'Matrícula', 'required|is_unique[usuarios.matricula]');
        }
    }

    public function buscar_usuarios() {
        $input = $this->input->get();
        $dados["input"] = $input;
        $paginacao = $this->get_query_paginacao($this->input->get('page'));
        if (!empty($input['busca'])) {
            $dados["resultados"] = $this->usuario->buscar_usuario_secretaria_setor($input['busca'], $paginacao);
            $this->initPagination('buscar-usuarios', $this->usuario->total_busca($input['busca']));
        } else {
            $dados["resultados"] = [];
        }
        $this->carregar_pagina('usuario/buscar_usuarios', $dados);
    }

    public function manual_do_usuario() {
        header("Content-Type: application/pdf");
        readfile(FCPATH . "assets/manual_do_usuario.pdf");
    }

}
