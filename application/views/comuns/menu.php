<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=base_url('assets/img/logoalertfire.png')?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=nome()?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">NEVEGAÇÃO</li>
            <li><a href="<?=base_url('mapa')?>"><i class="fa fa-map"></i> <span>Mapa de incêndio</span></a></li>
            <?php if(verifica_perfil('admin')):?>
            <li><a href="<?=base_url('chamados')?>"><i class="fa fa-fire"></i> <span>Ver chamados</span></a></li>
            <?php endif;?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>