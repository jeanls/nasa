<?php

function formatar_data($date_time)
{
    return date("d/m/Y", strtotime($date_time));
}

function formatar_data_mysql($date_time)
{
    return $date_time != null ? date("Y-m-d", strtotime($date_time)) : null;
}

function formatar_datetime_mysql($date_time)
{
    return $date_time != null ? date("Y-m-d H:i:s", strtotime($date_time)) : null;
}

function formatar_datetime($date_time)
{
    return date("d/m/Y H:i:s", strtotime($date_time));
}

function formatar_ano($date_time)
{
    return date("Y", strtotime($date_time));
}

function formatar_hora($date_time)
{
    return $date_time != null ? date("H:i:s", strtotime($date_time)) : null;
}

function formatar_datetime_local($date_time)
{
    return date("Y-m-d\TH:i:s", strtotime($date_time));
}

function buscar_query_url()
{
    return http_build_query($_GET, '', "&");
}

function adicionar_alerta($tipo, $msg)
{
    $CI = &get_instance();
    $alertas = $CI->session->flashdata('alertas');
    $novo = array(
        'class' => "$tipo",
        'mensagem' => "$msg"
    );
    
    if ($alertas != null) {
        array_push($alertas, $novo);
    } else {
        $alertas[] = $novo;
    }
    
    $CI->session->set_flashdata('alertas', $alertas);
}

function username()
{
    $CI = &get_instance();
    return $CI->session->userdata('username');
}

function nome()
{
    $CI = &get_instance();
    return $CI->session->userdata('name');
}

function url_image()
{
    $CI = &get_instance();
    $CI->load->model('usuario');
    $usuario = $CI->usuario->buscar_row(['where' => ['id' => $CI->session->user_id]]);
    if(!empty($usuario->avatar)){
        return base_url($usuario->avatar);
    }else{
        return '';
    }
}

function formata_valor($valor){
    return number_format($valor, 2, ',', '.');
}

function get_total_carrinho(){
    $itens = get_carrinho();
    $total = 0;
    foreach ($itens as $item){
        $total += $item->valor;
    }
    return $total;
}

function get_carrinho(){
    $CI = &get_instance();
    $item_ids = $CI->session->itens;
    if($item_ids){
        $CI->load->model('item');
        $CI->load->model('evento');
        $itens = $CI->item->buscar(['where_in' => ['id' => $item_ids]]);
        foreach ($itens as $item){
            $item->evento = $CI->evento->buscar_row(['where' => ['id' => $item->evento_id]]);
        }
        return $itens;
    }
    return [];
}

function enviar_email($destinatario, $assunto, $corpo){
    $CI = &get_instance();
    $CI->load->library('email');
    $CI->email->from('suporte.gea@gmail.com', 'GEA - Gerênciador de Eventos Acadêmicos');
    $CI->email->to($destinatario);
    $CI->email->subject($assunto);
    $CI->email->message($corpo);
    return $CI->email->send();
}

function format_boolean($boolean, $true, $false)
{
    return $boolean == 1 ? $true : $false;
}

function get_array_select($itens, $id = "id", $nome = "nome")
{
    $result = [];
    foreach ($itens as $item) {
        $result[$item->{$id}] = $item->{$nome};
    }
    return $result;
}

function get_selected_array($itens, $id = "id")
{
    $result = [];
    foreach ($itens as $item) {
        $result[] = $item->{$id};
    }
    return $result;
}

function get_ids($items)
{
    $result = [];
    foreach ($items as $item) {
        $result[] = $item->id;
    }
    return $result;
}

function verifica_controller($controllers)
{
    $CI = &get_instance();
    return in_array($CI->router->class, $controllers) ? 'active' : '';
}

function formatar_datetime_no_sec($date_time) {
    return date("d/m/Y H:i", strtotime($date_time));
}

function formatar_date_friendly($date_time){
    $meses = ['01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro'];
    $mes = $meses[date("m", strtotime($date_time))];
    $ano = date("Y", strtotime($date_time));
    $dia = date("d", strtotime($date_time));
    return "$dia $mes de $ano";
}

function formatar_datetime_friendly($date_time){
    $meses = ['01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro'];
    $mes = $meses[date("m", strtotime($date_time))];
    $ano = date("Y", strtotime($date_time));
    $dia = date("d", strtotime($date_time));
    $hora = date("H", strtotime($date_time));
    $minuto = date("i", strtotime($date_time));
    return "$dia $mes de $ano as ".$hora."h$minuto";
}

function formatar_time_friendly($date_time){
    $hora = date("H", strtotime($date_time));
    $minuto = date("i", strtotime($date_time));
    return $hora."h$minuto";
}

function usuario_logado(){
    $CI = &get_instance();
    $CI->load->model('usuario');
    $usuario = $CI->usuario->buscar_row(['where' => ['id' => $CI->session->user_id]]);
    return $usuario;
}

function verifica_perfil($perfil)
{
    $CI = &get_instance();
    $CI->load->model('usuario');
    $perfis = get_array_select($CI->usuario->getPerfis($CI->session->user_id));
    return in_array($perfil, $perfis);
}

function verifica_perfil_setor($perfil, $setor)
{
    $CI = &get_instance();
    $CI->load->model('usuario');
    $perfis = get_array_select($CI->usuario->getPerfis($CI->session->user_id));
    $usuario = $CI->usuario->buscar_com_relacoes([
        "where" => [
            "id" => $CI->session->user_id
        ]
    ])[0];
    $permissao = $usuario->setor->nome == $setor ? true : false;
    return in_array($perfil, $perfis) && $permissao;
}

function atualiza_local()
{
    setlocale(LC_TIME, 'pt_BR', 'pt_BR.UTF-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Bahia');
}

function data_extensa($data)
{
    $meses = array(
        '01' => 'janeiro',
        '02' => 'fevereiro',
        '03' => 'março',
        '04' => 'abril',
        '05' => 'maio',
        '06' => 'junho',
        '07' => 'julho',
        '08' => 'agosto',
        '09' => 'setembro',
        '10' => 'outubro',
        '11' => 'novembro',
        '12' => 'dezembro'
    );
    
    $temp = explode("/", formatar_data($data));
    
    return $temp[0] . " de " . $meses[$temp[1]] . " de " . $temp[2];
}

function api_response($status, $codigo)
{
    $resposta = array(
        'sucesso' => $status,
        'protocolo' => $codigo
    );
    return json_encode($resposta);
}

function api_response_protocolo($protocolo)
{
    $resposta = array(
        'status' => $protocolo->status
    );
    return json_encode($resposta);
}
function remover_assinatura_redundante($assinaturas){
    $CI = &get_instance();
    unset($assinaturas[$CI->session->user_id]);
    return $assinaturas;
}

function data_vazia($data){
    $chars = str_split($data);
    $count = 0;
    foreach ($chars as $char){
        if($char == '0'){
            $count++;
        }
    }
    return $count == 6 ? true : false;
}

function get_qtd_nao_visualizados(){
    $CI = &get_instance();
    $CI->load->model('usuario');
    $CI->load->model('protocolo');
    $usuario = $CI->usuario->buscar_row(['where' => ['id' => $CI->session->user_id]]);
    return $CI->protocolo->get_total_nao_visualizados($usuario->id, $usuario->setor_id);
}

function get_nao_visualizados(){
    $CI = &get_instance();
    $CI->load->model('usuario');
    $CI->load->model('protocolo');
    $usuario = $CI->usuario->buscar_row(['where' => ['id' => $CI->session->user_id]]);
    return $CI->protocolo->get_nao_visualizados($usuario->id, $usuario->setor_id);
}

//RASCUNHOS
function get_qtd_rascunhos_nao_visualizados(){
    $CI = &get_instance();
    $CI->load->model('usuario');
    $CI->load->model('rascunho');
    return $CI->rascunho->get_total_nao_visualizados($CI->session->user_id);
}

function get_rascunhos_nao_visualizados(){
    $CI = &get_instance();
    $CI->load->model('usuario');
    $CI->load->model('rascunho');
    return $CI->rascunho->get_nao_visualizados($CI->session->user_id);
}

function get_usuarios_vinculados(){
    $CI = &get_instance();
    $CI->load->model('usuario_vinculo');
    $vinculos = $CI->usuario_vinculo->get_vinculos($CI->session->user_id);
    return $vinculos;
}

function ordena_assinaturas($assinaturas){
    $CI = &get_instance();
    $CI->load->model('usuario');
    foreach ($assinaturas as $assinatura){
        $perfis = $CI->usuario->getPerfis($assinatura->id);
        foreach ($perfis as $perfi){
            $assinatura->peso = $perfi->peso;
        }
    }
    usort($assinaturas, function($a, $b) {
        return $a->peso < $b->peso;
    });
    return $assinaturas;
}

function get_protocolos_carrinho() {
    $CI = &get_instance();
    $CI->load->model('protocolo');
    $protocolo_ids = $CI->session->userdata('protocolos');
    if($protocolo_ids){
        return $CI->protocolo->buscar(['where_in' => ['id' => $protocolo_ids]]);
    }else{
        return [];
    }
}

function get_setores(){
    $CI = &get_instance();
    $CI->load->model('setor');
    return $CI->setor->buscar_array(['where' => ['ativo' => 1]]);
}

function no_carrinho($item_id){
    foreach (get_carrinho() as $item){
        if($item->id == $item_id){
            return true;
        }
    }
    return false;
}