<script>
    window.onload = function (ev) {
        <?php foreach ($dados as $dado):?>
        $("#remover-ent-<?=$dado->id?>").click(function (e) {
            e.preventDefault();
            swal({
                title: 'Você tem certeza?',
                text: "Esta operação não poderá ser desfeita!",
                type: 'error',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger',
                confirmButtonText: 'Sim, apagar!',
                cancelButtonText: 'Cancelar'
            }).then(function (result) {
                if (result) {
                    window.location.href = $("#remover-ent-<?=$dado->id?>")[0].href;
                }
            });
        });
        <?php endforeach;?>
    }
</script>