<?php if ($this->session->flashdata('alertas')): ?>
    <?php foreach ($this->session->flashdata('alertas') as $alerta): ?>
        <div class="alert alert-<?=$alerta['class']?> alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Aviso!</h4>
            <?=$alerta['mensagem']?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>