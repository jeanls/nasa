<?php
class MY_Controller extends CI_Controller {

    public function __construct($params = null) {
        parent::__construct();
        if(!isset($params)){
            $this->load->library("auth");
        }
        $this->load->database();
    }

    public function view($pagina, $dados = array()) {
        $data['conteudo'] = $this->load->view($pagina, $dados, true);
        $this->load->view('comuns/header', $dados);
        $this->load->view('comuns/layout', $data);
        $this->load->view('comuns/footer');
    }

    protected function initPagination($url, $total) {
        $config = array(
            'total_rows' => $total,
            'base_url' => base_url($url),
        );
        $this->pagination->initialize($config);
    }

    protected function get_query_paginacao($page) {
        $this->config->load('pagination');
        $paginacao = array(
            'pagina' => $page ? $page : 0,
            'quantidade' => $this->config->item('per_page'),
        );
        return $paginacao;
    }

    protected function redirect_current(){
        if (isset($_SERVER['HTTP_REFERER'])) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect('/');
        }
    }
}
