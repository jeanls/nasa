<?php

$config['status-protocolo'] = [
    'novo' => 'Novo',
    'em-andamento' => 'Em andamento',
    'reencaminhado' => 'Reencaminhado',
    'em-analise' => 'Em análise',
    'aguardando-resposta' => 'Aguardando resposta externa',
    'indeferido' => 'Indeferido',
    'resolvido' => 'Resolvido',
    'arquivado' => 'Arquivado',
    'aguardando' => 'Aguardando Resposta',
    'anexado' => 'Anexado a um processo',
];
$config['prioridades'] = [
    'baixa' => 'Baixa',
    'media' => 'Média',
    'alta' => 'Alta',
    'urgente' => 'Urgente',
];
$config['cor-prioridade'] = [
    'baixa' => 'hidden',
    'media' => 'hidden',
    'alta' => 'texto-warning',
    'urgente' => 'texto-danger',
];
