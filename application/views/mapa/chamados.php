<!-- Main content -->
<section class="content">
    <?php $this->view('comuns/alertas'); ?>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="box-title">Lista de Chamados</h3>
                </div>
                <div class="col-md-4">
                    <div class="box-tools">

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover table-bordered">
                <tbody><tr>
                    <th>Cód</th>
                    <th>Usuário</th>
                    <th>Endereço Estimado</th>
                    <th>Opção</th>
                </tr>
                <?php foreach ($itens as $item): ?>
                    <tr>
                        <td><?= $item->id ?></td>
                        <td><?= @$item->usuario->nome ?></td>
                        <td><?= $item->endereco ?></td>
                        <td>
                            <a href="<?=base_url('mapa_c/resolvido/'.$item->id)?>" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Resolvido</a>
                            <a href="<?=base_url('mapa_c/ver_mapa/'.$item->id)?>" class="btn btn-xs btn-warning"><i class="fa fa-map"></i> Ver no mapa</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</section>