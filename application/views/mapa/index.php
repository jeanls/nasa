<style>
    #googleMap {
        position: absolute;
        width: 100%;
        left: 50px;
        height: 100%;
    }

    @media only screen and (max-width: 691px) {
        #googleMap {
            left: 0;
        }
    }
</style>
<div id="googleMap"></div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?=APPNAME?></h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 20px">
                    <div class="col-md-12">
                        <h4>Qual tipo de ocorrência?</h4>
                        <input type="text" name="address1" readonly id="address" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-4 col-md-2">
                        <form method="post" action="<?=base_url('mapa_c/save')?>">
                            <input type="hidden" id="lat1" value="" name="lat">
                            <input type="hidden" id="lng1" value="" name="lng">
                            <input type="hidden" name="endereco" value="" id="address1">
                            <input type="hidden" name="tipo_ocorrencia" value="1">
                            <button type="submit" style="background-color: #f4511e;border: 2px #FFF;width: 85px" class="btn btn-primary"><i class="fa fa-fire fa-2x"></i><br>Incêncidio</button>
                        </form>
                    </div>
                    <div class="col-md-2">
                        <form method="post" action="<?=base_url('mapa_c/save')?>">
                            <input type="hidden" id="lat2" value="" name="lat">
                            <input type="hidden" id="lng2" value="" name="lng">
                            <input type="hidden" name="endereco" value="" id="address2">
                            <input type="hidden" name="tipo_ocorrencia" value="2">
                            <button type="submit" style="background-color: #fb8c00;border: #fff 2px;width: 85px" class="btn btn-primary"><i class="fa fa-fire fa-2x"></i><br>Foco</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<script>
    var map;
    function myMap() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                initMap(position.coords.latitude, position.coords.longitude);
            });
        } else {
            initMap(-12.973647, -38.505337);
        }

    }

    function initMap(latitude, longitude){
        var geocoder = new google.maps.Geocoder();
        var mapProp= {
            center:new google.maps.LatLng(latitude, longitude),
            zoom:12,
        };
        map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
        new google.maps.Marker({
            position:new google.maps.LatLng(latitude, longitude),
            map:map,
            icon: '<?=base_url('assets/img/user.png')?>'
        });
        var result = JSON.parse('<?=$result?>');


        for(var i = 0; i < result.length; i++){
            var marker = new google.maps.Marker({
                position:new google.maps.LatLng(result[i].lat,result[i].lng),
                map:map,
                icon: result[i].tipo_ocorrencia == 1 ? '<?=base_url('assets/img/fire.png')?>' : '<?=base_url('assets/img/foco.png')?>'
                //animation:google.maps.Animation.BOUNCE
            });

            var content = "<h4>Local: </h4>" + result[i].endereco;
            content += "<br>";
            content += "<h4>Tipo: </h4>";
            content += result[i].tipo_ocorrencia === '1' ? 'Incêncio' : 'Foco';
            content += "<br>";
            content += "<h4>Reportado por: </h4>";
            content += result[i].usuario;
            var infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', (function(marker,content,infowindow){
                return function() {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker,content,infowindow));
        }

        var nasa = JSON.parse('<?=$nasa?>');
        //console.log(nasa);
        for(var i = 0; i < nasa.length; i++){
            var marker = new google.maps.Marker({
                position:new google.maps.LatLng(nasa[i].latitude, nasa[i].longitude),
                map:map,
                icon: '<?=base_url('assets/img/satellite.png')?>'
                //animation:google.maps.Animation.BOUNCE
            });

            var content = "<h4>Fornecido por FIRMS (NASA)</h4><br><p>Detecção de fogo</p>";
            var infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', (function(marker,content,infowindow){
                return function() {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker,content,infowindow));
        }

        google.maps.event.addListener(map, 'click', function(event) {
            geocoder.geocode({
                'latLng': event.latLng
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        $('#address').val(results[1].formatted_address);

                        $('#lat1').val(event.latLng.lat);
                        $('#lng1').val(event.latLng.lng);
                        $('#address1').val(results[1].formatted_address);

                        $('#lat2').val(event.latLng.lat);
                        $('#lng2').val(event.latLng.lng);
                        $('#address2').val(results[1].formatted_address);
                        $('#myModal').modal('toggle');
                        //alert(results[1].formatted_address);
                    } else {
                        alert('Resultado não encontrado');
                    }
                } else {
                    alert('Não foi possível determinar a posição');
                }
            });
        });
    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6t8ePLXlhuxXe9eH8oINCRsEORNLjsaM&callback=myMap" async defer></script>