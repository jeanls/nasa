<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Identificador
 * @property Inscricao_seq inscricao_seq
 */

class Identificador{
    private $CI;
    private $ano;
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('inscricao_seq');
        $this->ano = date('Y');
    }

    public function get_sequencia(){
        $id = $this->CI->inscricao_seq->inserir(['data_cadastro' => date("Y-m-d H:i:s")]);
        return 'GEA' . $id . '/' . $this->ano;
    }
}