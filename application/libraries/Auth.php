<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {

    private $CI;
    private $permissions;
    private $exceptions;

    public function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('usuario');
        $this->CI->config->load('permissions');
        $this->permissions = $this->CI->config->item('permission');
        $this->exceptions = $this->CI->config->item('exceptions');
        $user = $this->CI->session->userdata('user_id');
        if (isset($user)) {
            if (!$this->check_permission($this->CI->router->class, $this->CI->router->method, $user)) {
                redirect('acesso-negado');
            }
        } else {
            if (!$this->check_exceptions($this->CI->router->class, $this->CI->router->method)) {
                redirect('login');
                return;
            }
        }
    }

    public function getRolesCurrentUser() {
        if (isset($this->CI)) {
            $user = $this->CI->session->userdata('user_id');
            return $this->CI->Usuario->getPerfis($user);
        }
        return array();
    }

    private function check_permission($class, $method, $user) {
        $class = strtolower($class);
        $method = strtolower($method);
        $roles = $this->CI->usuario->getPerfis($user);
        $controllers = array_keys($this->permissions);
        foreach ($controllers as $controller) {
            if ($controller == $class) {
                $methods = array_keys($this->permissions[$class]);
                foreach ($methods as $action) {
                    if (strtolower($action) == strtolower($method)) {
                        foreach ($this->permissions[$class][$action] as $role) {
                            if (strtolower($role) == strtolower('todos')) {
                                return true;
                            }
                            foreach ($roles as $r) {
                                if ($r->nome == $role) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    function check_exceptions($classe, $metodo) {
        $classe = strtolower($classe);
        $metodo = strtolower($metodo);
        if (isset($this->exceptions[$classe])) {
            if (in_array($metodo, $this->exceptions[$classe])) {
                return true;
            }
        }
        return false;
    }

}
