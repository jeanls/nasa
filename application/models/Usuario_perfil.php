<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_perfil extends MY_Model {
    public function __construct(){
        parent::__construct();
        $this->tabela = "usuario_perfil";
    }

    public function removeAll($id){
        $this->db->delete("usuario_perfi", "usuario_id = $id");
    }

    public function perfis($usuario_id, $full){
        $usuario_perfis = $this->db->select("perfil_id")->from($this->tabela)->where("usuario_id", $usuario_id)->get()->result();
        $perfis = array();
        foreach($usuario_perfis as $usuario_perfil){
            $result = $this->db->select("*")->from("perfil")->where("id", $usuario_perfil->perfil_id)->get()->row();
            if($full){
                $perfis[$result->id] = $result->nome;
            }else{
                array_push($perfis, $result->id);
            }
        }
        return $perfis;
    }
}