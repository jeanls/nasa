<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Mapa_c
 * @property Chamado chamado
 * @property Usuario usuario
 */
class Mapa_c extends MY_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('chamado');
        $this->load->model('usuario');
    }

    public function index(){
        $data = $this->chamado->buscar(['where' => ['aberto' => 1]]);
        $coordenadas = [];
        foreach ($data as $item){
            $usuario = $this->usuario->buscar_row(['where' => ['id' => $item->usuario_id]]);
            $coordenadas[] = ['lat' => $item->lat, 'lng' => $item->lng, 'tipo_ocorrencia' => $item->tipo_ocorrencia,
                'endereco' => $item->endereco, 'usuario' => $usuario->nome];
        }
        $json_nasa = file_get_contents('assets/data.json');
        $json_nasa = json_decode($json_nasa);
        $json_nasa = array_slice($json_nasa, 0, 20);
        $data['result'] = json_encode($coordenadas);
        $data['nasa'] = json_encode($json_nasa);
        $this->view('mapa/index', $data);
    }

    public function ocorrencias(){
        $this->view('mapa/ocorrencias');
    }

    public function chamados(){
        $terms = $this->input->get('q');
        $query = ['where' => ['aberto' => 1]];
        $paginacao = $this->get_query_paginacao($this->input->get('page'));
        $data['itens'] = $this->chamado->buscar($query, $paginacao);
        foreach ($data['itens'] as $item){
            $item->usuario = $this->usuario->buscar_row(['where' => ['id' => $item->usuario_id]]);
        }
        $this->initPagination('chamados', $this->chamado->total($query));
        $this->view('mapa/chamados', $data);
    }

    public function resolvido($chamado_id){
        $this->chamado->atualizar($chamado_id, ['aberto' => 0]);
        adicionar_alerta('success', 'Chamado arquivado com sucesso');
        redirect('chamados');
    }

    public function ver_mapa($chamado_id){
        $data['result'] = $this->chamado->buscar_row(['where' => ['id' => $chamado_id]]);
        $data['usuario'] = $this->usuario->buscar_row(['where' => ['id' => $data['result']->usuario_id]]);
        $data['result'] = json_encode($data['result']);
        $this->view('mapa/ver_mapa', $data);
    }

    public function save(){
        $data = $this->input->post(['lat', 'lng', 'endereco', 'tipo_ocorrencia']);
        $data['data_cadastro'] = date('Y-m-d H:i:s');
        $data['usuario_id'] = usuario_logado()->id;
        $this->chamado->inserir($data);
        redirect('mapa');
    }
}