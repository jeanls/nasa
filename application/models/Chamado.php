<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chamado extends MY_Model {
    public function __construct(){
        parent::__construct();
        $this->tabela = 'chamado';
        $this->ordenacao = [
            'id' => 'asc',
        ];
    }
}